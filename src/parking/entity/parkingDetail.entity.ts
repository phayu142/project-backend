import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ParkingDetail {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({ default: null, length: 30 })
  name: string;

  @Column({ default: null, length: 255 })
  location: string;

  @Column('datetime')
  ceate_date?: Date;

  @Column({ type: "datetime", default: null })
  update_date?: Date;
}