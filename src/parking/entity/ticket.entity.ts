import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Ticker {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  parking_id: string;

  @Column({ default: null})
  pd_id: string;
  
  @Column({ default: null, length: 30 })
  registration_plate: string;

  @Column({ default: null, length: 30 })
  size: string

  @Column('datetime')
  parking_time?: Date;

  @Column({ type: "datetime", default: null })
  leave_parking_time?: Date;
}