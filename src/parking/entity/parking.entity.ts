import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Parking {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  pd_id: string;


  @Column({ default: null, length: 30 })
  size: string;

  @Column({ default: null, scale: 15 })
  nearest_sequence: number

  @Column({ length: 30 })
  status: string

  @Column('datetime')
  ceate_date?: Date;

  @Column({ type: "datetime", default: null })
  update_date?: Date;
}