import { Test, TestingModule } from '@nestjs/testing';
import { now } from 'moment-timezone';
import { identity } from 'rxjs';
import { ParkingController } from './parking.controller';
import { ParkingService } from './parking.service';

describe('ParkingController', () => {
    let parkingController: ParkingController;
    const mockParkingService = {
        registrationParking: jest.fn( ) ,
        // leaveParking: jest.fn().mockImplementation((id, dto) => ({
        //     id,
        //     ...dto
        // })).mockImplementation()
        leaveParking:jest.fn()
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ParkingController],
            providers: [ParkingService]

        })
            .overrideProvider(ParkingService)
            .useValue(mockParkingService)
            .compile();

        parkingController = module.get<ParkingController>(ParkingController);
    });

    it('should be defined', () => {
        expect(parkingController).toBeDefined();
    });

    it('should registration parking', () => {
        const dto = {
            registrationPlate: "ฺBKK",
            size: "small",
        }
        // expect(parkingController.registrationParking(dto)).toEqual(dto)

        // expect(mockParkingService.registrationParking).toHaveBeenCalledWith(dto);
    });




})