import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  HttpStatus,
  HttpCode,
  Put,
} from '@nestjs/common';
// import { CreateParkingDto } from './dto/create-album.dto';
import { ParkingDto, ceateParking, registrationParking, leaveParking, statusParking, dataBySize } from './dto/parking.dto'
import { ParkingService } from './parking.service';
import { Parking } from './entity/parking.entity';
import { get } from 'http';

@Controller('parking')
export class ParkingController {
  constructor(private readonly parkingService: ParkingService) { }

  @Get()
  getHello(): string {
    return this.parkingService.getHello();
  }

  @Post('/create-parking')
  async ceateParking(@Body() reqData: ceateParking) {
    let ceateParking = await this.parkingService.ceateParing(reqData);
    return ceateParking;
  }

  @Post('/registration-parking')
  async registrationParking(@Body() reqData: registrationParking) {
    let sizeCar = (reqData.size).toLowerCase();
    if ((sizeCar !== "small") && (sizeCar !== "medium") && (sizeCar !== "large")) {
      return { statusCode: 400, message: 'bad request' };
    }

    let registrationParking = await this.parkingService.registrationParking(reqData);
    return registrationParking;
  }

  @Put('/leave-parking')
  async leaveParking(@Body() reqData: leaveParking) {
    let leaveParking = await this.parkingService.leaveParking(reqData);
    return leaveParking;
  }

  @Get('/status-parking/:pdId')
  async getStatusParking(@Param() reqData: statusParking) {
    let statusParking = await this.parkingService.getStatusPraking(reqData);
    return statusParking;
  }

  @Get('/registration-plate')
  async getRegistrationPlateBySize(@Body() reqData: dataBySize) {
    let data = await this.parkingService.getRegistrationPlateBySize(reqData);
    return data;
  }


  @Get('/registration-slot')
  async getRegistrationSlot(@Body() reqData: dataBySize) {
    let data = await this.parkingService.getRegistrationSlot(reqData);
    return data;
  }




}
