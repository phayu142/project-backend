import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Parking } from './entity/parking.entity';
import { ParkingDetail } from './entity/parkingDetail.entity';
import { createQueryBuilder } from 'typeorm';
import { v4 as uuid } from 'uuid';
import * as moment from 'moment-timezone';
import { getRepository } from "typeorm";
import { Ticker } from './entity/ticket.entity';


var dateNow = moment.tz(new Date(), "Asia/Bangkok").format('YYYY-MM-DD HH:mm:ss');

@Injectable()
export class ParkingService {


  getHello(): string {
    return 'Hello Worldfdsssfs!';
  }

  async ceateParing(reqData) {
    try {
      let smallTotal = reqData.small.total;
      let mediumTotal = reqData.medium.total;
      let largeTotal = reqData.large.total;
      let smallNamePosition = reqData.small.namePosition;
      let mediumNamePosition = reqData.medium.namePosition;
      let largeNamePosition = reqData.large.namePosition;
      let pdId = uuid();
      let detail: any = {
        id: pdId,
        name: reqData.nameLocation,
        location: reqData.location,
        ceate_date: dateNow,
      }
      let paringDetailInsert = await createQueryBuilder()
        .insert()
        .into(ParkingDetail)
        .values(detail)
        .execute()

      for (let i = 0; i < mediumTotal; i++) {
        let data: any = {
          id: uuid(),
          size: 'medium',
          nearest_sequence: mediumNamePosition[i],
          status: "free",
          ceate_date: dateNow,
          pd_id: pdId

        }
        let parkingMediumTotal = await createQueryBuilder()
          .insert()
          .into(Parking)
          .values(data)
          .execute()

      }
      for (let i = 0; i < smallTotal; i++) {
        let data: any = {
          id: uuid(),
          size: 'small',
          nearest_sequence: smallNamePosition[i],
          status: "free",
          ceate_date: dateNow,
          pd_id: pdId

        }
        let parkingSmall = await createQueryBuilder()
          .insert()
          .into(Parking)
          .values(data)
          .execute()

      }
      for (let i = 0; i < largeTotal; i++) {
        let data: any = {
          id: uuid(),
          size: 'large',
          nearest_sequence: largeNamePosition[i],
          status: "free",
          ceate_date: dateNow,
          pd_id: pdId

        }
        let parkingLarge = await createQueryBuilder()
          .insert()
          .into(Parking)
          .values(data)
          .execute()
      }
      return { statusCode: 200, message: 'insert data success' }
    } catch (err) {
      return { statusCode: 500, message: 'insert data fail', error: err.message }
    }
  }

  async registrationParking(reqData) {
    try {
      let sizeCar = (reqData.size).toLowerCase();
      const resultSizeCar = await getRepository(Parking)
        .createQueryBuilder("p")
        .select([
          "p.id as id",
          "p.size as size",
          "p.nearest_sequence as nearestSequence",
          "p.status as status",
          "p.pd_id as pdId"
        ])
        .where("p.size = :size", { size: sizeCar })
        .andWhere("p.status != :status", { status: "unavailable" })
        .orderBy(`p.nearest_sequence`, 'ASC')
        .limit(1)
        .getRawMany()

      if (resultSizeCar.length == 0) {
        return { statusCode: 204, message: 'size parking is full' }
      }

      let tikerId = uuid();
      let parkingTime = dateNow;
      let data: any = {
        id: tikerId,
        parking_id: resultSizeCar[0].id,
        registration_plate: reqData.registrationPlate,
        size: sizeCar,
        parking_time: parkingTime,
        pd_id: resultSizeCar[0].pdId,

      }
      let tikerParkin = await createQueryBuilder()
        .insert()
        .into(Ticker)
        .values(data)
        .execute()

      let updateStatus = await createQueryBuilder()
        .update(Parking)
        .set({
          status: "unavailable"
        })
        .where(`id in (:id)`, { id: resultSizeCar[0].id })
        .execute()

      let responseData: any = {
        id: tikerId,
        pId: resultSizeCar[0].id,
        nearestSequence: resultSizeCar[0].nearestSequence,
        size: resultSizeCar[0].size,
        parkingTime: parkingTime
      }

      return { statusCode: 200, message: `get data success`, data: responseData }

    } catch (error) {
      return { statusCode: 500, message: 'insert data fail', error: error.message }

    }
  }

  async leaveParking(reqData) {
    try {
      let updateStatus = await createQueryBuilder()
        .update(Parking)
        .set({
          status: "free",
          update_date: dateNow,
        })
        .where(`id in (:id)`, { id: reqData.pId })
        .execute()

      let leaveParking = await createQueryBuilder()
        .update(Ticker)
        .set({
          leave_parking_time: dateNow
        })
        .where(`id in (:id)`, { id: reqData.id })
        .execute()

      return { statusCode: 200, message: `update data success`, }
    } catch (error) {
      return { statusCode: 500, message: 'update data fail', error: error.message }
    }

  }
  async getStatusPraking(reqData) {
    try {

      const resultData = await getRepository(Parking)
        .createQueryBuilder("p")
        .select([
          "p.id as id",
          "p.size as size",
          "p.nearest_sequence as nearestSequence",
          "p.status as status"
        ])
        .where("p.pd_id = :pdId", { pdId: reqData.pdId })
        .orderBy(`p.size`, 'ASC')
        .getRawMany()
      
      if (resultData.length == 0) {
        return { statusCode: 204, message: `No Content`, data: resultData }
      }
      return { statusCode: 200, message: `get data success`, data: resultData }
    } catch (error) {
      return { statusCode: 500, message: 'get data fail', error: error.message }
    }

  }
  async getRegistrationPlateBySize(reqData) {
    try {
      const resultData = await getRepository(Ticker)
        .createQueryBuilder("t")
        .select([
          "t.id as id",
          "t.registration_plate as registrationplate",
          "t.size as size",
          "t.parking_time as parkingTime"

        ])
        .where("t.pd_id = :pdId", { pdId: reqData.pdId })
        .andWhere("t.size = :size", {size :reqData.size})
        .orderBy(`t.size`, 'ASC')
        .getRawMany()
      
      if (resultData.length == 0) {
        return { statusCode: 204, message: `No Content`, data: resultData }
      }
      return { statusCode: 200, message: `get data success`, data: resultData }
    } catch (error) {
      return { statusCode: 500, message: 'get data fail', error: error.message }
    }

  }
  async getRegistrationSlot(reqData) {
    try {
      const resultData = await getRepository(Parking)
        .createQueryBuilder("p")
        .select([
          "p.id as id",
          "p.size as size",
          "p.nearest_sequence as nearestSequence",
          "p.status as status",
          "p.ceate_date as ceateDate"

        ])
        .where("p.pd_id = :pdId", { pdId: reqData.pdId })
        .andWhere("p.size = :size", {size :reqData.size})
        .orderBy(`p.ceate_date`, 'ASC')
        .getRawMany()
      
      if (resultData.length == 0) {
        return { statusCode: 204, message: `No Content`, data: resultData }
      }
      return { statusCode: 200, message: `get data success`, data: resultData }
    } catch (error) {
      return { statusCode: 500, message: 'get data fail', error: error.message }
    }

  }

}
