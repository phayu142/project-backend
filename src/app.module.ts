import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ParkingModule } from './parking/parking.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Parking } from './parking/entity/parking.entity';
import { Ticker } from './parking/entity/ticket.entity';
import { ParkingDetail} from './parking/entity/parkingDetail.entity'

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: './app.sqlite',
      entities: [Parking,Ticker,ParkingDetail],
      synchronize: process.env.NODE_ENV != 'production',
    }),
    ParkingModule],
  controllers: [AppController],
  providers: [AppService],
  
})
export class AppModule { }
